import java.util.ArrayList;
import java.util.Collections;


public class Board{	
	private char [][] board;													
	private char player;															
	private final int SIZE = 8;													
	private final Disk diskSet;													
	private double [][] heuristicMatrix;										
		
	public Board() {
		board = new char[SIZE][SIZE];											
		diskSet = Disk.getDiskInst();											
		heuristicMatrix = new double[SIZE][SIZE];								
	}
	
	private boolean outOfBoard(int row,int column) {
		return (row < 0 || column < 0 || row >= SIZE || column >= SIZE);
	}
	//GETTER AND SETTER ATTRIBUTES
	public char[][] getBoard() {
		return board;
	}
	public void setBoard(char[][] board) {
		this.board = board;
	}
	public char getPly() {
		return player;
	}
	public void setPly(char player) {
		this.player = player;
	}
	public double[][] getHeuristicMatrix() {
		return heuristicMatrix;
	}

	public void setHeuristicMatrix(double[][] heuristicMatrix) {
		this.heuristicMatrix = heuristicMatrix;
	}
	// Checks if the user has made the right move in accordance with the rules
	/**
	* @param source_row															
	* @param source_column															
	* @return an integer reflecting whether the move made is correct or not				
	*/
	public int getWrongMove(char [][] _board,int _row,int _col,char player){
		int rowNext, colNext, row, column;
		char opponent = (player == diskSet.getWhite())?diskSet.getBlack():diskSet.getWhite();
		//condition to avoid placing over other disks
		if(_board[_row][_col] != diskSet.getEmpty()){
			return 0;
		}
		else{
			//check if placing next to the current tiles. Random placement on the board should not be allowed
			for (rowNext = -1 ; rowNext <= 1 ; rowNext++) {
				for (colNext = -1 ; colNext <= 1 ; colNext++) {
					row = _row + rowNext;
					column = _col + colNext;
					if(outOfBoard(row, column) || (rowNext == 0 && colNext == 0)) {
						continue;
					}
					if(_board[row][column] == opponent){
						System.out.println("\n\nopponent check if\n\n");
						return 1;
					}
					else{
						continue;
					}
				}
			}
		}
		return 0;
	}
	
	/**
	* @param player																	
	* @param board															
	* @return the candidate children that are all possible moves for me				
	*/
	private ArrayList<Child> findAllPossibleMoves(char player, char [][] _board,boolean max) {
		int _row,_col,row_lookahead,col_lookahead,row,column;
		//	clear any previous moves and move paths;
		ArrayList<Child> children = new ArrayList<Child>();
		char opponent = (player == diskSet.getWhite())?diskSet.getBlack():diskSet.getWhite();
		//	for each block on the board, check if it can be a candidate move for the current player
		for (_row = 0 ; _row < SIZE ; _row++) {
			for(_col = 0 ; _col < SIZE ; _col++) {
				//	if the current position is occupied, it can not be a candidate for next player
				if(_board[_row][_col] != diskSet.getEmpty()){
					continue;
				}
				//	if current position is empty, check the adjacent blocks for compatibility in every direction, 
				//	if compatibility is found in any direction, mark the cell as compatible and break search in that direction. 
				for (row_lookahead = -1 ; row_lookahead <= 1 ; row_lookahead++) {
					for (col_lookahead = -1 ; col_lookahead <= 1 ; col_lookahead++) {
						//	next adjacent row,column
						row = _row+row_lookahead;
						column = _col+col_lookahead;
						//	no need to check outside the board or on the current position
						if(outOfBoard(row, column) || (row_lookahead == 0 && col_lookahead == 0)) {
							continue;
						}
						if(_board[row][column] == opponent){
							//	if an opponent is found on one of the adjacent boxed, check deeper
							while(true) {
								row += row_lookahead;
								column += col_lookahead;
								if(outOfBoard(row, column)) {
									break;
								}
								if(_board[row][column] == diskSet.getEmpty()) {
									break;
								}
								//	if any such condition occurs down the search path - opponent{1}opponent{0,}player{1,} 
								//	--> means current position is a candidate move
								if(_board[row][column] == player) {
									//	possible candidate move -> including duplicates
									//	spawn a child
									if(max)
										children.add(new Child(_row, _col, row, column,Double.POSITIVE_INFINITY));
									else
										children.add(new Child(_row, _col, row, column,Double.NEGATIVE_INFINITY));
									break;
								}
							}	
						}
					}
				}
			}
		}
		return children;
	}
	
	/**
	* @param player																	
	* @param source_row															
	* @param source_col															
	* @return a shadow of board state with the new move being played				
	*/
	public char[][] changeBoardStateFor(char player, int child_row,int child_col, char [][] _board) {
		int rowNext,colNext,row,column;
		char opponent = player == diskSet.black?diskSet.white:diskSet.black;
		//	create a temporary lookahead board instance.
		char [][] childState = copyByValue(_board);
		//	set the current block to player
		childState[child_row][child_col] = player;
		//	flip the disks that get captured
		for (rowNext = -1 ; rowNext <= 1 ; rowNext++) {
			for (colNext = -1 ; colNext <= 1 ; colNext++) {
				row = child_row + rowNext;
				column = child_col + colNext;
				if(outOfBoard(row, column) || (rowNext == 0 && colNext == 0)) {
					continue;
				}
				if (childState[row][column] == opponent) {
					while (true) {
						row += rowNext;
						column += colNext;					
						if(outOfBoard(row, column)) {
							break;
						}
						if(childState[row][column] == diskSet.empty) {
							break;
						}
						if(childState[row][column] == player) {
							//	a move has been detected
							//	traverse in the reverse direction and make all the opponent => player
							row -= rowNext;
							column -= colNext;
							while(childState[row][column] == opponent) {
								childState[row][column] = player;
								row -= rowNext;
								column -= colNext;
							}
							break;
						}
					}
				}
			}
		}
		// the new board instance with the move been made.
		return childState;
	}
	/**
	* @param board																																
	* @return a temp board				
	*/
	private char[][] copyByValue(char[][] _board) {
		char boardInstance[][] = new char[SIZE][SIZE];
		for (int i = 0 ; i < SIZE ; i++) {
			for (int j = 0 ; j < SIZE ; j++) {
				boardInstance[i][j] = _board[i][j];
			}
		}
		return boardInstance;
	}
	
	//MINIMAX
	public Child getChild_MiniMax(char player, char [][] _board, int height, boolean max, Child parent) {
		char tempState[][];
		double _eval;
		Child forkChild = null,_bestChild;
		ArrayList<Child> children;
		Child dummy = null;
		//	find possible children for board state = '_board' for player = player
		children = findAllPossibleMoves(player, _board, max);
		//	if no children found  - player cannot make any move, try for the opponent
		if(children.size() == 0) {
			// try to find children states of opponent
			children = findAllPossibleMoves(player == diskSet.black?diskSet.white:diskSet.black, _board, !max);
			if(children.size() == 0) {
				//set score for a dummy move spawn in case no other move is available
				Child spawnChild = null;
				int myCount = playerCount(diskSet.getMe(),_board);
				int opCount = playerCount(diskSet.getMe()==diskSet.black?diskSet.white:diskSet.black, _board);
				boolean endCase = myCount == 0 || opCount == 0;
					
				if(max) {
					spawnChild = new Child(SIZE, SIZE, SIZE, SIZE, Double.POSITIVE_INFINITY);
				}
				else {
					spawnChild = new Child(SIZE, SIZE, SIZE, SIZE, Double.NEGATIVE_INFINITY);
				}						
				if(height > 1) {
					forkChild = getChild_MiniMax(player ==diskSet.black?diskSet.white:diskSet.black, _board, height-1, !max, spawnChild);
					spawnChild.setMoveEval(forkChild.getMoveEval());
					if (max && (parent.getMoveEval() < spawnChild.getMoveEval())) {
						parent.setMoveEval(spawnChild.getMoveEval());
					}
					if (!max && (parent.getMoveEval() > spawnChild.getMoveEval())) {
						parent.setMoveEval(spawnChild.getMoveEval());
					}
				}
				else {
					_eval = evaluateBoard(diskSet.getMe(), _board);
					spawnChild.setMoveEval(_eval);
					
					if (max && (parent.getMoveEval() < spawnChild.getMoveEval())) {
						parent.setMoveEval(spawnChild.getMoveEval());
					}
					if (!max && (parent.getMoveEval() > spawnChild.getMoveEval())) {
						parent.setMoveEval(spawnChild.getMoveEval());
					}
				}
				
				
				return spawnChild;
			}
			else {
				//	player could not find the move, he passed, opponent found the move
				if(max) {
					dummy = new Child(SIZE, SIZE, SIZE, SIZE, Double.POSITIVE_INFINITY);
				}
				else{
					dummy = new Child(SIZE, SIZE, SIZE, SIZE, Double.NEGATIVE_INFINITY);
				}
				children = new ArrayList<Child>();
				children.add(dummy);
			}
				
		}
		for (Child child : children) {
			
			if(child.getRow() != SIZE){
				//	create a temporary child state with this child
				tempState = changeBoardStateFor(player, child.getRow(), child.getCol(), _board);
			}
			else{
				tempState = _board;
			}
			if(height > 1) {
				forkChild = getChild_MiniMax(player==diskSet.black?diskSet.white:diskSet.black, tempState, height-1, !max,child);
			}
			if(height == 1) {
				_eval = evaluateBoard(diskSet.getMe(), tempState);
				child.setMoveEval(_eval);
				if (max && (parent.getMoveEval() < child.getMoveEval())) {
					parent.setMoveEval(child.getMoveEval());
				}
				if (!max && (parent.getMoveEval() > child.getMoveEval())) {
					parent.setMoveEval(child.getMoveEval());
				}
				
			}
			else {
				child.setMoveEval(forkChild.getMoveEval());
				if(max && parent.getMoveEval() < child.getMoveEval()) {
					parent.setMoveEval(child.getMoveEval()); 
				}
				if(!max && parent.getMoveEval() > child.getMoveEval()) {
					parent.setMoveEval(child.getMoveEval()); 
				}
				
			}
		}
		Collections.sort(children);
		if(!max) {
			Collections.reverse(children);
		}
		_bestChild = children.get(0);
		parent.setMoveEval(_bestChild.getMoveEval());
		
		if(!outOfBoard(_bestChild.getRow(),_bestChild.getCol())) {
			_board = changeBoardStateFor(player, _bestChild.getRow(), _bestChild.getCol(), _board);
		}
		return _bestChild;
	}
	
	/**
	 * 
	 * @param player
	 * @param _board
	 * @return the count of player on the _board
	 */
	public int playerCount(char player, char[][] _board) {
		int i,j,count;
		count = 0;
		for (i = 0 ; i < SIZE ; i++) {
			for (j = 0 ; j< SIZE ; j++) {
				count = _board[i][j] == player?count+1:count;
			}
		}
		return count;
	}	
	
	
	public double evaluateBoard(char player, char _board[][]) {
		char opponent = player==diskSet.white?diskSet.black:diskSet.white;
		int _row,_col;
		double score = 0;
		for(_row = 0 ; _row < SIZE ; _row++) {
			for(_col = 0 ; _col < SIZE ; _col++){
				if(_board[_row][_col] == player) {
					score += heuristicMatrix[_row][_col];	
				}
				else if(_board[_row][_col] == opponent) {
					score -= heuristicMatrix[_row][_col];
				}
			}
		}
		return score;
	}
}