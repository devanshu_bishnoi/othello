import java.util.Scanner;
public class StartPlay {

        public static void main(String [] args) { 
								
                Board board = new Board ();

                char [][] state = { 	{'*', '*', '*', '*', '*', '*', '*', '*'},
                                        {'*', '*', '*', '*', '*', '*', '*', '*'},
                                        {'*', '*', '*', '*', '*', '*', '*', '*'},
                                        {'*', '*', '*', 'X', 'O', '*', '*', '*'},
                                        {'*', '*', '*', 'O', 'X', '*', '*', '*'},
                                        {'*', '*', '*', '*', '*', '*', '*', '*'},
                                        {'*', '*', '*', '*', '*', '*', '*', '*'},
                                        {'*', '*', '*', '*', '*', '*', '*', '*'}
                                };

                final double [][] positionalHeuristic = {

                                                {99,-8,8,6,6,8,-8,99},
                                                {-8,-24,-4,-3,-3,-4,-24,-8},
                                                {8,-4,7,4,4,7,-4,8},
                                                {6,-3,4,0,0,4,-3,6},
                                                {6,-3,4,0,0,4,-3,6},
                                                {8,-4,7,4,4,7,-4,8},
                                                {-8,-24,-4,-3,-3,-4,-24,-8},
                                                {99,-8,8,6,6,8,-8,99}
                                            }; 
				int height;
				
                board.setHeuristicMatrix(positionalHeuristic);

                board.setBoard(state);

                Scanner in = new Scanner(System.in);

                System.out.println("Start Game (y/n)?");
				                
				if (in.nextLine().trim().charAt(0) != 'n'){
					System.out.println("##SELECT DIFFICULTY LEVEL(type in the number to select difficulty)## | 1(Easy) | 2(Medium) | 3(Hard)");
					int level = in.nextInt();
					if(level == 1){
						System.out.println("EASY");
						height = 1;
					}
					else if(level == 2){
						System.out.println("MEDIUM");
						height = 3;
					}
					else{
						System.out.println("HARD (This is also the default choice in case you have entered value other than the ones mentioned!)");
						height = 5;
					}
					System.out.println("You take O, I take X, your turn first! BEGIN!");

					draw(state);

					char player = 'O';
					char me = 'X';
					int wrongMove = 0;
					int no_moves = 0;
					while (no_moves != 2) {
							if (player == 'O'){
									System.out.println("Your turn: O\n Tell me the place to flip the disk [row<space>column], If you have no remaining move, hit -1<space>-1, if you would like to quit anytime in the middle of the game hit -2<space>-2");
									int row = in.nextInt();
									int column = in.nextInt(); 
									if (row == -1 && column == -1){
											no_moves++;
											System.out.println("You skipped");
											if (no_moves == 2){
													double score1 = board.evaluateBoard('O',state);
													double score2 = board.evaluateBoard('X',state);
													System.out.println("Game End");
													System.out.println("\nYour Score = "+score1);
													System.out.println("\nMy Score = "+score2);
													if (score1 > score2) {
														System.out.println("\nYou Win");
													}
													else if(score1 == score2){
														System.out.println("\nDraw");
													}
													else{
														System.out.println("\nI Win");
													}
													break;
											}
									}
									else if (row == -2 && column == -2){
											System.out.println("You QUIT, I WIN");
											System.exit(0);
									}
									else{
											wrongMove = board.getWrongMove(state,row,column,player);
											if(wrongMove == 0){
												System.out.println("Either the position is not empty or there is no disk in proximity to the placement. Please place your disc at an empty position next to current disks");
												continue;
											}
											state = board.changeBoardStateFor('O', row, column, state);
											no_moves =0;
											System.out.println("Your moves outcome -->");
									}        
									draw(state);
									player = 'X';
							}
							else{
									System.out.println("My Turn: X");
									Child move = board.getChild_MiniMax('X', state, height, true, new Child(-1, -1,-1, -1,Double.NEGATIVE_INFINITY));
									if(move != null && move.getRow() != -1 && move.getRow() != 8){ 
											board.setBoard(board.changeBoardStateFor('X', move.getRow(),move.getCol(), state));
											state = board.getBoard();
											no_moves = 0;
											System.out.println("My moves outcome -->");
									}
									else {
											no_moves++;
											if (no_moves == 2){
													double score1 = board.evaluateBoard('O',state);
													double score2 = board.evaluateBoard('X',state);
													System.out.println("Game End");
													System.out.println("\nYour Score = "+score1);
													System.out.println("\nMy Score = "+score2);
													if (score1 > score2) {
														System.out.println("\nYou Win");
													}
													else if(score1 == score2){
														System.out.println("\nDraw");
													}
													else{
														System.out.println("\nI Win");
													}
													break;
											}
											System.out.println("I don't have a move");
									}
									draw(state);
									player = 'O';
							}       

					}

				}
				else{
					System.exit(0);
				}

        }

        private static void draw(char [][] board) {

                System.out.println("Board -->");
                System.out.println("# # # # # # # #");
                System.out.println();
                for (int i=-1; i<8; i++){
                        for (int j=-1; j < 8; j++){
                                if(i == -1 || j == -1){
                                        if(i == -1 && j == -1) {
                                                System.out.print("/  ");
                                        }
                                        else if (i == -1){
                                                System.out.print(j+"_");
                                        }

                                        else{
                                                System.out.print(i+"| ");
                                        }
                                }

                                else{
                                        System.out.print(board[i][j]+" ");
                                }
                        }
                        System.out.println();
                }
                System.out.println();
                System.out.println("# # # # # # # #");
        }

}