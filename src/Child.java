public class Child implements Comparable<Child>{	
	private int row;
	private int column;
	private int parent_row;
	private int parent_column;
	private double moveEval;
	
	Child() {
		
	}
	
	Child(int row,int column, int parent_row, int parent_column,double eval) {
		this.row = row;
		this.column = column;
		this.parent_row = parent_row;
		this.parent_column = parent_column;
		this.moveEval = eval;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {
		return column;
	}
	public void setCol(int column) {
		this.column = column;
	}
	public int getParent_row() {
		return parent_row;
	}
	public void setParent_row(int parent_row) {
		this.parent_row = parent_row;
	}
	public int getParent_column() {
		return parent_column;
	}
	public void setParent_column(int parent_column) {
		this.parent_column = parent_column;
	}
	public double getMoveEval() {
		return moveEval;
	}
	public void setMoveEval(double moveEval) {
		this.moveEval = moveEval;
	}
	
	public int compareTo(Child o) {
		if (this.moveEval > o.getMoveEval()) {
			return -1;
		}
		else if(this.moveEval < o.getMoveEval()) {
			return 1;
		}
		else {
			if (this.row < o.getRow()) {
				return -1;
			}
			else if (this.row > o.getRow()) {
				return 1;
			}
			else {
				if(this.column < o.getCol()) {
					return -1;
				}
				else if(this.column > o.getCol()) {
					return 1;
				}
				else {
					return 0;
				}	
			}
		}	
	}	
}